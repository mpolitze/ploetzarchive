#import scrapy
#
#class BlogSpider(scrapy.Spider):
#    name = 'blogspider'
#    start_urls = ['https://www.ploetzblog.de/archiv']
#
#    def parse(self, response):
#        #for title in response.css('.oxy-post-title'):
#        #    yield {'title': title.css('::text').get()}
#
#        for next_page in response.css('.contenttable a'):
#            log(next_page)
#            if next_page[-3:] == 'pdf':
#                yield response.follow(next_page, self.parse)

#!/usr/bin/env python3

import scrapy

class MySpider(scrapy.Spider):

    name = 'myspider'

    start_urls = [
          'https://www.ploetzblog.de/archiv',
    ]

    def parse(self, response):

        for link in response.css('.contenttable a').xpath('@href').extract():
             url = response.url
             #path = response.css('ol.breadcrumb li a::text').extract()
             next_link = response.urljoin(link)
             #next_link = link
             yield scrapy.Request(next_link, callback=self.parse_det, meta={'url': url})

    def parse_det(self, response):

        def extract_with_css(query):
            return response.css(query).get(default='').strip()

        yield {
            'path':response.meta['path'],
            'file_urls': [extract_with_css('a.btn.btn-primary::attr(href)')],
            'url':response.meta['url']
        }


from scrapy.crawler import CrawlerProcess

c = CrawlerProcess({
    'USER_AGENT': 'Mozilla/5.0',

    # save in file as CSV, JSON or XML
    'FEED_FORMAT': 'csv',     # csv, json, xml
    'FEED_URI': 'output.csv', # 

    # download files to `FILES_STORE/full`
    # it needs `yield {'file_urls': [url]}` in `parse()`
    'ITEM_PIPELINES': {'scrapy.pipelines.files.FilesPipeline': 1},
    'FILES_STORE': '.',
})
c.crawl(MySpider)
c.start()
