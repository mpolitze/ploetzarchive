from datetime import date
import time
import os
import json

from playwright.sync_api import Page
from playwright.sync_api import sync_playwright

with open('out/_index.json', 'r') as fp:
    links = json.load(fp)

print(f'Index has {len(links.items())} entries.')

newFiles = 0

p = sync_playwright().start()
browser = p.chromium.launch(headless=True)

for l, d in links.items():
    filename = f'{d["slug"]}'
    if not os.path.exists(f'out/print/{filename}.pdf'):
        print(f'({newFiles:03d}) {filename} not found. Downloading.')
        newFiles += 1
        page = browser.new_page()
        page.goto(l)
        page.add_style_tag(content='[data-step-hidden="true"]{color: silver;}')
        page.evaluate('() => document.querySelector(".cleanslate")?.remove()')
        #page.evaluate('() => document.querySelector("#temperatureMode").click()')
        page.evaluate('() => document.querySelector("#percentageMode").click()')
        page.evaluate('() => document.querySelector("#beginnerMode").click()')

        with open(f'out/html/{filename}.html', 'w') as fp:
            fp.write(page.content())
        
        page.add_style_tag(content='img[alt~="Buchcover"] { display: none; }')

        page.pdf(path=f'out/print/{filename}.pdf', format='A4')
        
        page.add_style_tag(content='.module-p-5 .print\:module-hidden { display: grid; }')
        page.pdf(path=f'out/print-mod/{filename}.pdf', format='A4')

        page.emulate_media(media="screen")
        page.pdf(path=f'out/screen/{filename}.pdf', format='A4')

        page.evaluate('() => localStorage.clear()')

        page.close()

        time.sleep(2)

    if newFiles >= 100:
        print('Got 100 new files. Stopping for now.')
        break
