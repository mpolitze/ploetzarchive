import json
import os
import re
from urllib.request import urlopen
from urllib.parse import urlparse
from bs4 import BeautifulSoup as bs
from datetime import date
import time
from urllib.parse import unquote

months = ['', 'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember']

def parse_date(d):
  p = re.split(r'[\W]+', d)
  m = months.index(p[1])
  x = f'{p[2]}-{m:02d}-{p[0]}'
  return date.fromisoformat(x)

links = {}

if os.path.exists('out/_index.json'):
    with open('out/_index.json', 'r') as fp:
        links = json.load(fp)

lastrun = { 'page' : 1 }

if os.path.exists('out/_lastrun.json'):
    with open('out/_lastrun.json', 'r') as fp:
        lastrun = json.load(fp)

nextPage = lastrun['page']

while True:
    i = nextPage
    nextPage += 1

    pageUrl = f'https://www.ploetzblog.de/rezepte?sort=newest,0&sheet={i}#results'
    
    print(f'Now on page {i}')

    html = urlopen(pageUrl).read()
    html_page = bs(html, features="lxml")
    og_url = html_page.find("meta",  property = "og:url")
    base = urlparse(pageUrl)
    #print("base", base)

    divs = html_page.find_all('div', class_='we2p-result__content')

    print(f'  has {len(divs)} entries')

    if len(divs) == 0:
        nextPage -= 1
        break

    for div in divs:
        current_link = div.find_all('a')[0].get('href')
        current_link = unquote(current_link)
        description = div.find_all('p')[0].get_text().strip()
        rdate = div.find_all('p')[1].get_text().strip()
        rdate = parse_date(rdate)
        
        title = div.find_all('h4')[0].get_text().strip()

        slug = re.split('/', current_link)[-2]

        data = {'title': title, 'desc':description, 'slug':f'{rdate.isoformat()}_{slug}'}

        if current_link is not None and '/rezepte/' in current_link and 'id=' in current_link:
            if og_url:
                #print("currentLink",current_link)
                links[og_url["content"] + current_link] = data
            else:
                links[base.scheme + "://" + base.netloc + current_link] = data

    time.sleep(2)

with open('out/_index.json', 'w') as fp:
    json.dump(links, fp)

lastrun['page'] = nextPage - 1

with open('out/_lastrun.json', 'w') as fp:
    json.dump(lastrun, fp)
