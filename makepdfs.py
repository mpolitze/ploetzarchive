import requests
import validators
import sys
from bs4 import BeautifulSoup as bs
from urllib.parse import urlparse
import wget
from urllib.request import urlopen
import urllib.request
from datetime import datetime
from dateutil.parser import parse
import dateutil
import locale
import re
import os

from datetime import date
from playwright.sync_api import Page

from playwright.sync_api import sync_playwright



p = sync_playwright().start()
browser = p.chromium.launch(headless=True)
page = browser.new_page()

links = {}

months = ['', 'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember']

def parse_date(d):
    p = re.split(r'[\W]+', d)
    m = months.index(p[1])
    x = f'{p[2]}-{m:02d}-{p[0]}'
    return date.fromisoformat(x)

#locale.setlocale(locale.LC_TIME, ('de', 'UTF-8'))

for i in range(1,2):
    my_url = f'https://www.ploetzblog.de/rezepte?sort=newest,0&sheet={i}#results'

    html = urlopen(my_url).read()
    html_page = bs(html, features="lxml")
    og_url = html_page.find("meta",  property = "og:url")
    base = urlparse(my_url)
    print("base", base)

    for div in html_page.find_all('div', class_='we2p-result__content'):
        current_link = div.find_all('a')[0].get('href')
        description = div.find_all('p')[0].get_text().strip()
        rdate = div.find_all('p')[1].get_text().strip()
        rdate = parse_date(rdate)
        
        slug = re.split('/', current_link)[2]

        data = {'desc':description, 'slug':f'{rdate.isoformat()}_{slug}'} 

        if current_link is not None and '/rezepte/' in current_link and 'id=' in current_link:
            if og_url:
                #print("currentLink",current_link)
                links[og_url["content"] + current_link] = data
            else:
                links[base.scheme + "://" + base.netloc + current_link] = data

import json
with open('out/_index.json', 'w') as fp:
    json.dump(links, fp)


print(links)
print(len(set(links)))

for l, d in links.items():
    filename = f'out/{d["slug"]}'
    if not os.path.exists(f'{filename}.pdf'):
        page.goto(l)
        page.add_style_tag(content='[data-step-hidden="true"]{color: silver;}')
        page.evaluate('() => document.querySelector(".cleanslate").remove()')
        #page.evaluate('() => document.querySelector("#temperatureMode").click()')
        page.evaluate('() => document.querySelector("#percentageMode").click()')
        page.evaluate('() => document.querySelector("#beginnerMode").click()')
        with open(f'{filename}.html', 'w') as fp:
            fp.write(page.content())
        page.pdf(path=f'{filename}.pdf')

